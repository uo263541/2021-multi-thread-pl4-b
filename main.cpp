/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>
#include <stdlib.h>
#include <errno.h>

using namespace cimg_library;
void* ThreadProc(void* arg);
// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;
const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
//Image 2
const char* SOURCE2_IMG      = "background_V.bmp";
const unsigned short NUM_THREADS = 4;
uint repeticiones = 10;

data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
data_t* pRsrc2, * pGsrc2, * pBsrc2; //Second image
data_t *pRdest, *pGdest, *pBdest;
data_t *pDstImage; // Pointer to the new image pixels
uint width, height; // Width and height of the image
uint width2, height2; // Width and height of the image2
uint nComp; // Number of image components
uint size;
uint size_thread;

struct Estructura{
          //Variables for the start and the end
          uint start;
		  uint end;
          //Image 1
          data_t* pRsrc;
          data_t* pGsrc;
          data_t* pBsrc;
          //Image 2
          data_t* pRsrc2;
          data_t* pGsrc2;
          data_t* pBsrc2;
          //Dest Image
          data_t* pRdest;
          data_t* pGdest;
          data_t* pBdest;

};

int main() {

// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> srcImage2(SOURCE2_IMG);

	if(srcImage.is_empty() || srcImage2.is_empty()){
		exit(-2);
	}


	//Variables for the benchmark time	
	struct timespec tStart, tEnd;
	double dElapsedTimeS;

	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	//Image1
	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	//Image2
	srcImage2.display();
	width2  = srcImage2.width(); // Getting information from the source image
	height2 = srcImage2.height();
	
	
	if (width != width2 || height != height2) { //Compare image size
			printf("Dimensiones de im�genes no coincidentes");
			exit(-1);

		}

	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
	
	Estructura est[NUM_THREADS];
	//Size of the image
	size= width*height;  
	//Size for each thread
	size_thread= size/NUM_THREADS;
	
	
	
	
	//Loop to initialize the pointers to the images of the structure
	for (uint i = 0; i < NUM_THREADS; i++){
	

		// Pointers to the componet arrays of the source image
		est[i].pRsrc = srcImage.data(); // pRcomp points to the R component array
		est[i].pGsrc = est[i].pRsrc + width * height; // pGcomp points to the G component array
		est[i].pBsrc = est[i].pGsrc + width * height; // pBcomp points to B component array

		// Pointers image 2
		est[i].pRsrc2 = srcImage2.data(); // pRcomp points to the R component array
		est[i].pGsrc2 = est[i].pRsrc2 + width * height; // pGcomp points to the G component array
		est[i].pBsrc2 = est[i].pGsrc2 + width * height; // pBcomp points to B component array

		// Pointers to the RGB arrays of the destination image
		est[i].pRdest = pDstImage;
		est[i].pGdest = est[i].pRdest + width * height;
		est[i].pBdest = est[i].pGdest + width * height;

		//Variable inicio fin needed to split the size of each algorithm
		est[i].start=size_thread*i;
		est[i].end=size_thread*(i+1);
    
	}

	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */

	if(clock_gettime(CLOCK_REALTIME, &tStart)!=0){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	/************************************************
	 * TODO: Algorithm.
	 */
	
	pthread_t Threads[NUM_THREADS];
	for (uint j=0; j<repeticiones; j++){
		for (uint i = 0; i < NUM_THREADS; i++) {

			
			//creation of the threads
			int pthread_ret = pthread_create(&Threads[i], NULL, ThreadProc, &est[i]);
			if (pthread_ret != 0) {
				printf("ERROR: pthread_create error code: %d.\n", pthread_ret);
				exit(EXIT_FAILURE);
			}
			
			
		}
		//waiting for the threads
		for (uint i = 0; i < NUM_THREADS; i++) {
			pthread_join(Threads[i], NULL);
		}
		
	}

	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	 
	 if(clock_gettime(CLOCK_REALTIME, &tEnd)!=0){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time    : %f s.\n", dElapsedTimeS);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);

	return 0;
}

//method for the threads
void* ThreadProc(void* arg) {
	
	//Cast
	Estructura est= *((Estructura*)arg);
	
	


	for (uint i = est.start; i < est.end; i++) {

		*(est.pRdest + i) =   255 -  (256 * (( 255 - (*( est.pRsrc2 + i ) )))  / ( *( est.pRsrc + i ) +1));
		*(est.pGdest + i) =   255 -  (256 * (( 255 - (*( est.pGsrc2 + i ) )))  / ( *( est.pGsrc + i ) +1));
		*(est.pBdest + i) =   255 -  (256 * (( 255 - (*( est.pBsrc2 + i ) )))  / ( *( est.pBsrc + i ) +1));
		
			
		//check saturation
		if (*(est.pRdest + i) < 0) {
			*(est.pRdest + i) = 0;

		}
		if (*(est.pGdest + i) < 0) {
			*(est.pGdest + i) = 0;
		}
					
		if (*(est.pBdest + i) < 0) {
			*(est.pBdest + i) = 0;
		}
		
		if (*(est.pRdest + i) > 255) {
			*(est.pRdest + i) = 255;

		}
		if (*(est.pGdest + i) > 255) {
			*(est.pGdest + i) = 255;
		}
		
		if (*(est.pBdest + i) > 255) {
			*(est.pBdest + i) = 255;

		}
	}
	
	
	return 0;

}
